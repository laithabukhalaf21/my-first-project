const express=require('express');
const router=express.Router();
const information = require('../model/information');


router.get('/',function(req,res){
    res.send("hello world")
})

router.post('/info',function(req,res){
    const dbInfo = req.body

    information.create(dbInfo ,(err,data)=>{
         if(err){
             res.send(err)
         }else{
             res.send(data)
         }
    })
})

router.get('/infos',function(req,res){
    information.find((err,data)=>{
        if(err){
            res.send(err)
        }else{
            res.send(data)
        }

    })
})
















module.exports = router;