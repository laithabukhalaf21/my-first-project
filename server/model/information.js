const mongoose=require('mongoose')
const Schema=mongoose.Schema

const infoSchema=new Schema({
    name:String,
    surname:String,
    email:String,
    rating:Number,
    checked:Boolean
})

const infos=mongoose.model("informations",infoSchema)
module.exports=infos