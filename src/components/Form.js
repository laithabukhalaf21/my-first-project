import React, { Component } from "react";
import { Typography, Slider, Button } from "@material-ui/core";
import Checkbox from '@material-ui/core/Checkbox'
import "./Form.css";

class Form extends Component {
  constructor() {
    super();
    this.state = {
      name: "",
      surname: "",
      email: "",
      rating: 5,
      checked: false,
    };
  }

  cleanState() {
    this.setState({ name: "" });
    this.setState({ surname: "" });
    this.setState({ email: "" });
    this.setState({ rating: 5 });
    this.setState({ checked: false });
  }

  handleChange = (e) => {
    const target = e.target;
    let value = target.value;
    const name = target.name;
    this.setState({
      [name]: value,
    });
  };

   handleChange2 = (event) => {

    const target = event.target;
    this.setState({
      checked:target.checked,
      
    });
    
 
  };

   handleChange3 = (event, newValue) => {
    this.setState({ rating:newValue})
};

  

  pushtoState = () => {
    let x = { ...this.state };
    // console.log(x)
    this.props.save(x);
    console.log(x);
    this.cleanState();
  };
    handleChange3 = (event, newValue) => {
    this.setState({ rating:newValue})
};


   

  render() {
    return (
      <form className="contact">
        <fieldset>
          <div>
            <label htmlFor="Name">Name</label>
            <input
              value={this.state.name}
              type="text"
              size="35"
              id="Name"
              name="name"
              width="100"
              onChange={this.handleChange}
            />
          </div>

          <div>
            <label htmlFor="Surname">surname</label>
            <input
              value={this.state.surname}
              type="text"
              size="35"
              id="Surname"
              name="surname"
              onChange={this.handleChange}
            />
          </div>

          <div>
            <label htmlFor="Email">Email</label>
            <input
              value={this.state.email}
              type="text"
              size="35"
              id="Email"
              name="email"
              onChange={this.handleChange}
            />
          </div>
        </fieldset>

        <div className="slider">
          <Typography id="discrete-slider-custom" gutterBottom>
            How do you rate the Norsia logo?
          </Typography>

          <Slider
                    value={this.state.rating}
                    min={0}
                    step={1}
                    max={10}
                    onChange={this.handleChange3}
                    valueLabelDisplay="auto"
                    aria-labelledby="non-linear-slider"
                />
        </div>

        <div className="checkbox">
        <Typography>
          Check the box if you think we should change our logo.
          <Checkbox
            checked={this.state.checked}
            onChange={this.handleChange2}
            inputProps={{ "aria-label": "primary checkbox" }}
          />
          </Typography>
        </div>
        <Button
          onClick={this.pushtoState}
          variant="contained"
          color="primary"
          size="large"
        >
          Save
        </Button>

        
      </form>
    );
  }
}

export default Form;
