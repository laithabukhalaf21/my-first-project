import React, { Component } from "react";
class Info extends Component {
 
  render() {
    let t = this.props.infos;
    // console.log(t.checked)

    return (
      <div className="info">
        <span className="fields"> Name : {t.name}</span>
        <span className="fields"> Surname : {t.surname}</span>
        <span className="fields"> Email : {t.email}</span>
        <span className="fields"> Rating : {String(t.rating)}</span>
        <span className="fields"> Checked : {String(t.checked)}</span>
        
      </div>
    );
  }
}

export default Info;
