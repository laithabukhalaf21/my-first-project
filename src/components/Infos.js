import React, { Component } from "react";

import Info from "./Info";

class Infos extends Component{
   

    render(){
        let infos=this.props.infos
        
        
        return(
            <div className="infos">
                
                {infos.map(t => (<Info infos={t} />
          
          
        ))}
        
            </div>
        )
    }
}


export default Infos