import React, { Component } from 'react';
import './App.css';
import Infos from './components/Infos';
import axios from 'axios';
import Form from './components/Form';
const logo = require('../src/norsia-logo-sans-baseline-RGB.png')
class App extends Component {
 constructor(){
   super();
   this.state={
     infos:[]
   };
 }

  componentDidMount() {
    axios.get('http://localhost:4000/infos')
    .then(response =>{this.setState({infos:response.data})
    })
    .catch(error => {console.log(error)})
  }

  getInfo(){
    return axios.get('http://localhost:4000/infos')
  }

  save=async(info)=>{
    await axios.post('http://localhost:4000/info',info)
    let response=await this.getInfo()
    this.setState({infos:response.data})
    console.log(this.state)
  }


render(){

  return (
    <div className="App">

     <img src={logo} className="logo" alt="" />

     <Form getInfo={this.getInfo} save={this.save}
       infos={this.state.infos}
     />
     {/* <Checkboxes infos={this.state.infos}/> */}

     <Infos infos={this.state.infos}/>


    </div>
      
    
      
  )
}
}

export default App
